const express = require("express");
const hbs = require("hbs");
const mysql = require("mysql");
let app = express();

let establishments = {};
let departments = {};
let groups = {};
let users = {};

const config = {
  host: "localhost",
  user: "root",
  password: "",
  database: "universities"
};

app.get('/', (req, res) => {
  /*let routes = [
    {url:"/", name:"Home"}
  ];*/
  let routes = [];
  routes.push({url: "/", name: "Home"});
  const connection = mysql.createConnection(config);
  connection.connect(function(err) {
  if (err) throw err;
  connection.query("SELECT * FROM establishments", function (err, result, fields) {
    if (err) throw err;
    establishments = result;
    connection.destroy();
  res.render('home.hbs', {
    establishments: establishments,
    routes: routes
  });
  });
  });
  });

app.get('/establishments', (req, res) => {
  //  this.languages.push({title: title, description: description});
  let routes = [];
  routes.push({url: "/", name: "Home"}, {url: "/establishments", name: "Establishments"});
  const connection = mysql.createConnection(config);
  connection.connect(function(err) {
  if (err) throw err;
  connection.query("SELECT * FROM establishments", function (err, result, fields) {
    if (err) throw err;
    establishments = result;
    connection.destroy();
  res.render('establishments.hbs', {
    establishments: establishments,
    routes: routes
  });
});
});
});

app.get('/establishment/:id', (req, res) => {
  let routes = [];
  const connection = mysql.createConnection(config);
  let id = req.params.id;
  connection.connect(function(err) {
  if (err) throw err;
  connection.query(`SELECT * FROM departments where establishment_id = ${id}`, function (err, result, fields) {
    if (err) throw err;
    departments = result;
    connection.query("SELECT * FROM establishments", function (err, result, fields) {
      if (err) throw err;
      establishments = result;
      connection.query(`SELECT short_name FROM establishments where establishment_id = ${id}`, function (err, result, fields) {
        if (err) throw err;
        routes.push({url: "/", name: "Home"}, {url: `/establishment/${id}`, name: `${result[0].short_name}`});
        connection.destroy();
    res.render('departments.hbs', {
      departments: departments,
      establishments: establishments,
      routes: routes
    });
  });
});
});
});
});

app.get('/department/:id', (req, res) => {
  let routes = [];
  const connection = mysql.createConnection(config);
  let id = req.params.id;
  connection.connect(function(err) {
  if (err) throw err;
  connection.query(`SELECT * FROM groups where department_id = ${id}`, function (err, result, fields) {
    if (err) throw err;
    groups = result;
    connection.query("SELECT * FROM establishments", function (err, result, fields) {
      if (err) throw err;
      establishments = result;
      connection.query(`SELECT short_name FROM establishments where establishment_id = ${id}`, function (err, result, fields) {
        if (err) throw err;
        routes.push({url: "/", name: "Home"}, {url: `/establishment/${id}`, name: `${result[0].short_name}`});
        connection.query(`SELECT short_name FROM departments where department_id = ${id}`, function (err, result, fields) {
          if (err) throw err;
          routes.push({url: `/department/${id}`, name: `${result[0].short_name}`});
          connection.destroy();
    res.render('groups.hbs', {
        groups: groups,
        establishments: establishments,
        routes: routes
    });
  });
});
});
});
});
});

app.get('/group/:id', (req, res) => {
  let routes = [];
  const connection = mysql.createConnection(config);
  let id = req.params.id;
  connection.connect(function(err) {
  if (err) throw err;
  connection.query(`SELECT * FROM users where group_id = ${id}`, function (err, result, fields) {
    if (err) throw err;
    users = result;
    connection.query("SELECT * FROM establishments", function (err, result, fields) {
      if (err) throw err;
      establishments = result;
      connection.query(`SELECT short_name FROM establishments where establishment_id = ${id}`, function (err, result, fields) {
        if (err) throw err;
        routes.push({url: "/", name: "Home"}, {url: `/establishment/${id}`, name: `${result[0].short_name}`});
        connection.query(`SELECT short_name FROM departments where department_id = ${id}`, function (err, result, fields) {
          if (err) throw err;
          routes.push({url: `/department/${id}`, name: `${result[0].short_name}`});
          connection.query(`SELECT group_name FROM groups where group_id = ${id}`, function (err, result, fields) {
            if (err) throw err;
            routes.push({url: `/group/${id}`, name: `${result[0].group_name}`});
            connection.destroy();
    res.render('users.hbs', {
        users: users,
        establishments: establishments,
        routes: routes
    });
  });
});
});
});
});
});
});

app.set('view engine', 'hbs');

hbs.registerPartials(__dirname + '/views/partials');

app.listen(3000, () => {
  console.log("Example app listening on port 3000")
});
