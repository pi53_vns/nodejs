const express = require("express");
const mongoose = require("mongoose");

const userRouter = require('./routes/user');
//const taskRouter = require('./routes/task');

require('./db/mongoose');

let app = express();

app.use(express.json());
app.use(userRouter);
//app.use(taskRouter);

app.listen(3000, () => {
    console.log("Listening on 3000 port");
});
