const express = require("express");
const router = new express.Router();

const User = require('./../models/user');
const auth = require('./../middleware/auth');

//GetUsers
router.get("/users", async(req, res) => {
  try {
    let users = await User.find({});
    res.status(200).send(users);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

//GetUserById
router.get("/user/:id", async (req, res) => {
  try {
    let user = await User.findOne({_id: req.params.id});
    res.status(200).send(user);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

//CreateUser
router.post("/user", async(req, res) => {
    let user = new User(req.body);
    try {
      const token = await user.generateAuthToken();
      await user.save();
      res.status(201).send({user, token});
    } catch (e) {
      res.status(500).send(e.message);
    }
  });

  //UpdateUser
  router.patch("/users/:id", async(req, res) => {
    try {
      const user = await User.findById(req.params.id);
      if (!user) {
       res.status(404).send("Not Found");
      }
      const updates = ['name', 'email', 'password', 'age'];
      updates.forEach((update) => user[update] = req.body[update]);
      await user.save();
      res.status(200).send(user);
    } catch (e) {
      res.status(400).send(e.message);
    }
  });

  //LoginUser
  router.post("/users/login", async(req, res) => {
    try {
      const user = await User.findOneByCredentials(req.body.email, req.body.password);
      if (!user) {
       res.status(404).send("Not Found");
      }
      const token = await user.generateAuthToken();
      res.status(200).send({user, token});
    } catch (e) {
      res.status(400).send(e.message);
    }
  });

  router.get('/users/me', auth, async(req, res) => {
    res.send(req.user);
  });

  //LogoutUser
  router.post("/users/logout", auth, async(req, res) => {
    try {
      req.user.tokens = req.user.tokens.filter((token) => {
        return token.token != req.token;
      });
      await req.user.save();
      res.status(200).send("Logged out");
    } catch (e) {
      res.status(500).send();
    }
  });

  //DeleteUser
  router.delete("/user/:id/", async(req, res) => {
    try {
      let user = await User.findOneAndDelete({_id: req.params.id});
      if (!user) {
        res.status(404).send("Not Found");
      }
      res.status(200).send(user);
    } catch (e) {
      res.status(400).send(e.message);
    }
  });

module.exports = router;
