const express = require("express");
const hbs = require("hbs");
const yargs = require("yargs");
const request = require("request");
const fs = require("fs");

let app = express();

let data = fs.readFileSync("weather.json");
let cities = JSON.parse(data);

app.get('/weather', (req, res) => {
  res.render('weather.hbs', {
    cities: cities,
    weather: null
  });
});

app.get('/weather/:city', (req, res) => {
  let city = req.params.city;
  let weather = {};
   request ({
     url: `http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=e0a46969d09be9746235ebfb716c0ef1`},
      (error, response, body) => {
        let obj = JSON.parse(body);
        weather.country = `${obj.sys.country}`;
        weather.city = `${obj.name}`;
        weather.weather = `${obj.weather[0].description}`;
        weather.longitude = `Longitude: ${obj.coord.lon} `;
        weather.latitude = `Latitude: ${obj.coord.lat}`;
        weather.temperature = `${Math.floor(obj.main.temp)} degrees`;
        weather.pressure = `${obj.main.pressure} mm Hg`;
        weather.humidity = `${Math.floor(obj.main.humidity)} %`;
        weather.windSpeed = `${obj.wind.speed}`;
        res.render('weather.hbs', {
          cities: cities,
          weather: weather
    });
  });
});

app.set('view engine', 'hbs');

hbs.registerPartials(__dirname + '/views/partials');

hbs.registerHelper('list', function(context, options) {
  var ret = "<ul class=\"list-group\">";
  for (var i = 0; i < context.length; i++) {
    ret = ret + "<li class=\"list-group-item\">" + options.fn(context[i]) + "</li>";
  }
  return ret + "</ul>";
});

app.listen(3000, () => {
  console.log("Weather App listening on port 3000")
});
