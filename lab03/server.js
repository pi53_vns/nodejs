const express = require("express");
const hbs = require("hbs");
let app = express();

let person = {
  firstName: "Nataliia",
  lastName: "Volynets"
};

let people = [
  {url: "http://john-doe.com", firstName: "John", lastName: "Doe"},
  {url: "http://alex-freeman.com", firstName: "Alex", lastName: "Freeman"},
  {url: "http://peter-johnson.com", firstName: "Peter", lastName: "Johnson"}
];

app.get('/', (req, res) => {
  res.render('home.hbs', {
    pageContent: "Hello, this is Home Page",
    author: person,
    people: people
  });
});

app.get('/about', (req, res) => {
  res.render('about.hbs');
});

app.get('/weather', (req, res) => {
  res.send("This is a Weather Page");
});

app.get('/weather/:city', (req, res) => {
  let city = req.params.city;
  res.send(city);
});

app.get('/login', (req, res) => {
  res.send("This is a Login Page");
});

app.set('view engine', 'hbs');

hbs.registerPartials(__dirname + '/views/partials');


hbs.registerHelper('fullName', (person) => {
  return `${person.firstName} ${person.lastName}`;
});

hbs.registerHelper('siteTitle', (siteTitle) => {
  return "Some Title";
});

hbs.registerHelper('copyright', (copyright) => {
  return "Copyright";
});

hbs.registerHelper('currentYear', (currentYear) => {
  return new Date().getFullYear();
});

hbs.registerHelper('bold', function(options) {
  return `<strong>${options.fn(this)}</strong>`;
});

hbs.registerHelper('list', function(context, options) {
  var ret = "<ul>";
  for (var i = 0; i < context.length; i++) {
    ret = ret + "<li>" + options.fn(context[i]) + "</li>";
  }
  return ret + "</ul>";
});

app.listen(3000, () => {
  console.log("Example app listening on port 3000")
});
