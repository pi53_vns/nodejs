//Task 4
/*let user = {
    firstname: '',
    lastname: '',
    init: function (firstname, lastname){
      this.firstname = firstname;
      this.lastname = lastname;
    },
    printHello: function () {
        console.log(`Hello, ${this.firstname} ${this.lastname}`);
    }
};

module.exports = {
  init: user.init,
  printHello: user.printHello
};*/


//Task 9-11
const fs = require("fs");
const lodash = require("lodash");

let userString = fs.readFileSync("user-data.json");
let user = JSON.parse(userString);
user.addLanguage = function (title, description) {
  this.languages.push({title: title, description: description});
  fs.writeFileSync("user-data.json", JSON.stringify(this));
};

user.getLanguage = function (title) {
  var description = lodash.find(user.languages, {title: title});
  console.log(description.description);
}

user.getAllLanguages = function () {
  console.log(user.languages);
}

user.removeLanguage = function (title) {
  var deletedItem = lodash.findIndex(user.languages, {title: title});
  this.languages.splice(deletedItem, 1);
  fs.writeFileSync("user-data.json", JSON.stringify(this));
}

module.exports.user = user;
