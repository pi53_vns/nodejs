//Task 4
/*const user = require("./user-module.js");

user.init("John", "Doe");
user.printHello();*/

//Task 11
const data = require("./user-module.js");
let u = data.user;

const yargs = require("yargs");
let myArgs = yargs.argv;
let command = myArgs._[0]; //add, remove, list, read

switch(command) {
  case "add":
    u.addLanguage(myArgs.title, myArgs.description);
    break;
  case "read":
    u.getLanguage(myArgs.title);
    break;
  case "list":
    u.getAllLanguages();
    break;
  case "remove":
    u.removeLanguage(myArgs.title);
    break;
  default:
    console.log("Incorrect command");
}

//Task 7
/*const lodash = require('lodash');

//_.times : Invokes the iteratee n times, returning an array of the results of each invocation.
//Вызывает итератор n раз, возвращая массив результатов каждого вызова.
function Greetings() {
  return user.printHello();
};

let times = lodash.times(1, Greetings);

//_.find : Instead iterating through an array with a loop to find a specific object, we can simply use _.find.
// You can also find an object using multiple properties with a single line of code.
// Вместо того, чтобы пройтись по массиву с помощью цикла в поисках определенного объекта, можно просто использовать _.find.
// Также можно найти объект, используя множественные свойства в единой строке кода.

var users = [
  { firstName: "John", lastName: "Doe", age: 28, gender: "male" },
  { firstName: "Jane", lastName: "Doe", age: 5, gender: "female" },
  { firstName: "Jim", lastName: "Carrey", age: 54, gender: "male" },
  { firstName: "Kate", lastName: "Winslet", age: 40, gender: "female" }
];

var find = lodash.find(users, { lastName: "Doe"});
console.log(find);

//_.deburr : It removes all “combining diacritical marks”.
//Убирает все диакритические знаки (черточки над буквами).

console.log(lodash.deburr("déjà vu"));
console.log(lodash.deburr("Juan José"));

//_.cloneDeep : Clones the object.
//Клонирует объект. В отличие от _.clone, клон объекта будет иметь новый адрес в памяти.

var objects = [{ 'a': 1 }, { 'b': 2 }];

console.log(lodash.cloneDeep(objects));

//_.sortedUniq : This is used for the sorted arrays. All duplicated values won’t be returned.
//Используется для отсортированных массивов. Возвращает только уникальные значения.

var sortedArray = [1, 1, 2, 3, 3, 3, 5, 8, 8];
console.log(lodash.sortedUniq(sortedArray));*/
