const fs = require("fs");
const yargs = require("yargs");
const request = require("request");

const argv = yargs
  .options ({
    c: {
      demand: false,
      alias: 'city',
      describe: 'City to Fetch Weather for',
      string: true
    },
    id: {
      demand: false,
      describe: 'City Id to Fetch Weather for',
      string: true
    },
    lon: {
      demand: false,
      alias: 'longitude',
      describe: 'Longitude to Fetch Weather for',
      string: true
    },
    lat: {
      demand: false,
      alias: 'latitude',
      describe: 'Latitude to Fetch Weather for',
      string: true
    }
  })
.help()
.alias('help', 'h')
.argv;

 let city = argv.c;
 let id = argv.id;
 let longitude = argv.lon;
 let latitude = argv.lat;
 let url;

if(argv.c) {
  url = `?q=${city}`;
}
else if(argv.id) {
  url = `?id=${id}`;
}
else if(argv.lat && argv.lon) {
  url = `?lat=${latitude}&lon=${longitude}`;
}
else {
  console.log("Error\nPlease, write a correct command. Use --h or --help to see all available commands.");
  return;
}

  request (
    {url: `http://api.openweathermap.org/data/2.5/weather${url}&appid=e0a46969d09be9746235ebfb716c0ef1`},
    ( error, response, body ) => {

      let obj = JSON.parse(body);
      if (obj.message == "city not found") {
        console.log("City not found. Please, try again.")
        return;
      }
      else if(obj.message) {
        console.log("Incorrect argument(s). Please, try again.")
        return;
      }
      else {
      console.log(`Country: ${obj.sys.country}`);
      console.log(`City: ${obj.name}`);
      console.log(`Longitude: ${obj.coord.lon}`);
      console.log(`Latitude: ${obj.coord.lat}`);
      console.log(`Weather: ${obj.weather[0].description}`);
      console.log(`Temperature: ${Math.floor(obj.main.temp)} degrees Fahrenheit`);
      console.log(`Pressure: ${obj.main.pressure} mm Hg`);
      console.log(`Humidity: ${Math.floor(obj.main.humidity)}%`);
      console.log(`Wind Speed: ${obj.wind.speed}`);
      }
    });
