const express = require("express");
const router = new express.Router();

const User = require('./../models/user');

//GetUsers
router.get("/users", async(req, res) => {
  try {
    let users = await User.find({});
    res.status(200).send(users);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

//GetUserById
router.get("/user/:id", async (req, res) => {
  try {
    let user = await User.findOne({_id: req.params.id});
    res.status(200).send(user);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

//CreateUser
router.post("/user", async(req, res) => {
    let user = new User(req.body);
    try {
      await user.save();
      res.status(201).send(user);
    } catch (e) {
      res.status(500).send(e.message);
    }
  });

  //UpdateUser
  router.patch("/users/:id", async(req, res) => {
    try {
      let user = await User.updateOne({_id: req.params.id}, req.body, {new: true, runValidators: true});
      if (!user) {
        res.status(404).send("Not Found");
      }
      if (user.nModified == 0 && user.n == 0 && user.ok == 0) {
        res.status(400).send("Bad Request: Data Is Invalid");
      }
      res.status(200).send(user);
    } catch (e) {
      res.status(500).send(e.message);
    }
  });

  //DeleteUser
  router.delete("/user/:id/", async(req, res) => {
    try {
      let user = await User.findOneAndDelete({_id: req.params.id});
      if (!user) {
        res.status(404).send("Not Found");
      }
      res.status(200).send(user);
    } catch (e) {
      res.status(400).send(e.message);
    }
  });

module.exports = router;
