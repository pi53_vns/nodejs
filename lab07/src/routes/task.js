const express = require("express");
const router = new express.Router();

const Task = require('./../models/task');

//GetTasks
router.get("/tasks", async(req, res) => {
  try {
    let tasks = await Task.find({});
    res.status(200).send(tasks);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

//GetTaskById
router.get("/task/:id", async (req, res) => {
  try {
    let task = await Task.findOne({_id: req.params.id});
    res.status(200).send(task);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

//CreateTask
router.post("/task", async(req, res) => {
  let task = new Task(req.body);
  try {
    await task.save();
    res.status(201).send(task);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

//UpdateTask
router.patch("/tasks/:id", async(req, res) => {
try {
  let task = await Task.updateOne({_id: req.params.id}, req.body, {new: true, runValidators: true});
  if (!task) {
    res.status(404).send("Not Found");
  }
  if (task.nModified == 0 && task.n == 0 && task.ok == 0) {
    res.status(400).send("Bad Request: Data Is Invalid");
  }
  res.status(200).send(task);
} catch (e) {
  res.status(500).send(e.message);
}
});

//DeleteTask
router.delete("/task/:id/", async(req, res) => {
try {
  let task = await Task.findOneAndDelete({_id: req.params.id});
  if (!task) {
    res.status(404).send("Not Found");
  }
  res.status(200).send(task);
} catch (e) {
  res.status(400).send(e.message);
}
});

module.exports = router;
