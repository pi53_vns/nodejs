const express = require("express");
const mongoose = require("mongoose");

require('./db/mongoose');

const User = require('./models/user');
const Task = require('./models/task');

let app = express();
const jsonParser = express.json();

app.listen(3000, () => {
    console.log("Listening on 3000 port");
});

let user = new User({name: 'Natasha', age: 20, email: 'EMAIL@gmail.com', password: 'pass123456'});

let task = new Task({description: 'Some description'});

/*user.save().then(() => {
  console.log(user);
}).catch((error) => {
  console.log(error);
});

task.save().then(() => {
  console.log(task);
}).catch((error) => {
  console.log(error);
});*/

app.get("/users", (req, res) => {
  User.find({}).then((users) => {
    res.status(200).send(users);
  }).catch((error) => {
    res.status(500).send();
  });
});

app.get("/user/:id", (req, res) => {
  const id = req.params.id;
  User.findOne({_id: id}).then((user) => {
    res.status(200).send(user);
  }).catch((error) => {
    res.status(500).send();
  });
});

/*app.post("/user", jsonParser, function(req, res) {
    if (!req.body) return res.sendStatus(400);
    const name = req.body.name;
    const age = req.body.age;
    const email = req.body.email;
    const password = req.body.password;
    let newUser = new User({name: name, age: age, email: email, password: password});

    /*User.insertOne(user, function(err, result) {
        if (err) {
            throw err;
            return;
        }
        res.send(user);
    });*/

    /*newUser.save().then(() => {
      console.log(newUser);
    }).catch((error) => {
      console.log(error);
    });
});*/
app.post("/user", jsonParser, function(req, res) {
    if (!req.body) return res.sendStatus(400);
    const name = req.body.name;
    const age = req.body.age;
    const email = req.body.email;
    const password = req.body.password;
    User.create({name: name, age: age, email: email, password: password}).then((user) => {
      res.status(200).send(user);
    }).catch((error) => {
      res.status(500).send();
    });
});

app.get("/tasks", (req, res) => {
  Task.find({}).then((tasks) => {
    res.status(200).send(tasks);
  }).catch((error) => {
    res.status(500).send();
  });
});

app.get("/task/:id", (req, res) => {
  const id = req.params.id;
  Task.findOne({_id: id}).then((task) => {
    res.status(200).send(task);
  }).catch((error) => {
    res.status(500).send();
  });
});

app.post("/task", jsonParser, function(req, res) {
    if (!req.body) return res.sendStatus(400);
    const description = req.body.description;
    const completed = req.body.completed;
    Task.create({description: description, completed: completed}).then((task) => {
      res.status(200).send(task);
    }).catch((error) => {
      res.status(500).send();
    });
});
