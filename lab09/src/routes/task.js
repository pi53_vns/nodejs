const express = require("express");
const router = new express.Router();

const Task = require('./../models/task');
const auth = require('./../middleware/auth');

//GetTasks
router.get("/tasks", auth, async(req, res) => {
  try {
    let tasks = await Task.find({owner: req.user.id});
    res.status(200).send(tasks);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

//GetTaskById
router.get("/task/:id", auth, async (req, res) => {
  try {
    let task = await Task.findOne({_id: req.params.id, owner: req.user.id});
    if (!task) {
      res.status(404).send("Not Found");
    }
    res.status(200).send(task);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

//CreateTask
router.post("/tasks", auth, async(req, res) => {
  const task = new Task({
    ...req.body,
    owner: req.user.id
  });
  try {
    await task.save();
    res.status(201).send(task);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

//UpdateTask
router.patch("/tasks/:id", auth, async(req, res) => {
try {
  let task = await Task.findOne({_id: req.params.id, owner: req.user.id});
  if (!task) {
    res.status(404).send("Not Found");
  }
  const updates = ['description', 'completed'];
  updates.forEach((update) => task[update] = req.body[update]);
  await task.save();  
  res.status(200).send(task);
} catch (e) {
  res.status(500).send(e.message);
}
});

//DeleteTask
router.delete("/task/:id/", async(req, res) => {
try {
  let task = await Task.findOneAndDelete({_id: req.params.id});
  if (!task) {
    res.status(404).send("Not Found");
  }
  res.status(200).send(task);
} catch (e) {
  res.status(400).send(e.message);
}
});

module.exports = router;
